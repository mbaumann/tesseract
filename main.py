"""Calculate and plot tesseract
"""

# python3 setup.py build_ext --inplace

import datetime
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

def rot_xy(phi):
    """Rotatation matrix for rotation around x-y plane
    """
    return [(1, 0, 0, 0), (0, 1, 0, 0), (0, 0, np.cos(phi), -np.sin(phi)),
            (0, 0, np.sin(phi), np.cos(phi))]


def rot_xz(phi):
    """Rotatation matrix for rotation around x-z plane
    """
    return [(1, 0, 0, 0), (0, np.cos(phi), 0, -np.sin(phi)), (0, 0, 1, 0),
            (0, np.sin(phi), 0, np.cos(phi))]


def rot_xw(phi):
    """Rotatation matrix for rotation around x-w plane
    """
    return [(1, 0, 0, 0), (0, np.cos(phi), -np.sin(phi), 0),
            (0, np.sin(phi), np.cos(phi), 0), (0, 0, 0, 1)]


def rot_yz(phi):
    """Rotatation matrix for rotation around y-z plane
    """
    return [(np.cos(phi), 0, 0, -np.sin(phi)), (0, 1, 0, 0), (0, 0, 1, 0),
            (np.sin(phi), 0, 0, np.cos(phi))]


def rot_yw(phi):
    """Rotatation matrix for rotation around y-w plane
    """
    return [(np.cos(phi), 0, -np.sin(phi), 0), (0, 1, 0, 0),
            (np.sin(phi), 0, np.cos(phi), 0), (0, 0, 0, 1)]


def rot_zw(phi):
    """Rotatation matrix for rotation around z-w plane
    """
    return [(np.cos(phi), -np.sin(phi), 0, 0),
            (np.sin(phi), np.cos(phi), 0, 0), (0, 0, 1, 0), (0, 0, 0, 1)]


def proj_xy(vertices_4d):
    """Project 4d vertex onto xy-plane

    Args:
        vertices_4d (list of lists): list of 4d-vertices

    Returns:
        list of lists: list of 2d-vertices
    """
    d = 2  #distance of light source
    vertices_2d = []
    assert np.shape(vertices_4d) == (16, 4), np.shape(vertices_4d)
    for vertex_4d in vertices_4d:
        assert np.shape(vertex_4d) == (4, ), np.shape(vertex_4d)
        proj_matrix = [[1 / (d - vertex_4d[3]), 0, 0, 0],
                       [0, 1 / (d - vertex_4d[3]), 0, 0]]
        assert np.shape(proj_matrix) == (2, 4)
        vertex_2d = np.dot(proj_matrix, np.transpose(vertex_4d))
        assert np.shape(vertex_2d) == (2, ), np.shape(vertex_2d)
        vertices_2d.append(vertex_2d)
    assert np.shape(vertices_2d) == (16, 2)
    return vertices_2d


# vertices: [(x1,y1), (x2,y2), ... (xn,yn)]
#  edges:[(v1,v2), (v1,v3), ... (vk,vn)]
def plot_tesseract_slider(vertices, edges):
    fig, ax = plt.subplots()

    xy_0 = 0
    xz_0 = 0
    xw_0 = np.pi / 16
    yz_0 = 0
    yw_0 = np.pi / 16
    zw_0 = np.pi/128

    lim = 2
    ax.set_xlim(-lim, lim)
    ax.set_ylim(-lim, lim)
    ax.set_aspect('equal', adjustable='box')
    ax.set_axis_off()

    fig.subplots_adjust(left=0, bottom=0.25)
    l = []
    for edge in edges:
        v0 = vertices(xy_0, xz_0, xw_0, yz_0, yw_0, zw_0)[edge[0]]
        v1 = vertices(xy_0, xz_0, xw_0, yz_0, yw_0, zw_0)[edge[1]]
        x_coord = [v0[0], v1[0]]
        y_coord = [v0[1], v1[1]]
        l.append(ax.plot(x_coord, y_coord, 'k')[0])

    axxy = plt.axes([0.25, 0.3, 0.65, 0.03])
    axxz = plt.axes([0.25, 0.25, 0.65, 0.03])
    axxw = plt.axes([0.25, 0.15, 0.65, 0.03])
    axyz = plt.axes([0.25, 0.20, 0.65, 0.03])
    axyw = plt.axes([0.25, 0.1, 0.65, 0.03])
    axzw = plt.axes([0.25, 0.05, 0.65, 0.03])

    sxy = Slider(axxy,
                 'xy',
                 -np.pi / 2,
                 np.pi / 2,
                 valinit=xy_0,
                 valstep=np.pi / 256)
    sxz = Slider(axxz, 'xz', 0, np.pi / 2, valinit=xz_0, valstep=np.pi / 16)
    sxw = Slider(axxw,
                 'xw',
                 -np.pi / 2,
                 np.pi / 2,
                 valinit=xw_0,
                 valstep=np.pi / 64)
    syz = Slider(axyz, 'yz', 0, np.pi / 2, valinit=yz_0, valstep=np.pi / 16)
    syw = Slider(axyw,
                 'yw',
                 -np.pi / 2,
                 np.pi / 2,
                 valinit=yw_0,
                 valstep=np.pi / 64)
    szw = Slider(axzw,
                 'zw',
                 -np.pi / 2,
                 np.pi / 2,
                 valinit=zw_0,
                 valstep=np.pi / 256)

    def update(_):
        xy = sxy.val
        xz = sxz.val
        xw = sxw.val
        yz = syz.val
        yw = syw.val
        zw = szw.val
        for idx, edge in enumerate(edges):
            v0 = vertices(xy, xz, xw, yz, yw, zw)[edge[0]]
            v1 = vertices(xy, xz, xw, yz, yw, zw)[edge[1]]
            x_coord = [v0[0], v1[0]]
            y_coord = [v0[1], v1[1]]
            l[idx].set_xdata(x_coord)
            l[idx].set_ydata(y_coord)
        fig.canvas.draw_idle()
        # xz_i = xz / np.pi * 16
        # yz_i = yz / np.pi * 16
        # fig.savefig('image_xz{:.0f}_yz{:.0f}.svg'.format(xz_i, yz_i))

    sxy.on_changed(update)
    sxz.on_changed(update)
    sxw.on_changed(update)
    syz.on_changed(update)
    syw.on_changed(update)
    szw.on_changed(update)

    plt.show()

# vertices: [(x1,y1), (x2,y2), ... (xn,yn)]
#  edges:[(v1,v2), (v1,v3), ... (vk,vn)]
def plot_tesseract_grid(vertices, edges):
    N = 8

    fig, ax_list = plt.subplots(N+1,N+1,figsize=(6,6))

    fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)

    xy = 0
    xw = np.pi/12
    yw = np.pi/12
    zw = np.pi/128

    # v0_debug = vertices(xy, np.pi/8, xw, np.pi/8, yw, zw)
    # v1_debug = vertices(xy, np.pi/8, xw, np.pi/8, yw, zw)


    for xz_i in range(N+1): # vertical
        xz = (xz_i-N/2) * np.pi/2/N

        for yz_i in range(N+1):# horizontal
            yz = (yz_i-N/2) * np.pi/2/N

            ax = ax_list[xz_i][yz_i]

            lim = 2.5
            ax.set_xlim(-lim, lim)
            ax.set_ylim(-lim, lim)
            ax.set_aspect('equal', adjustable='box')
            ax.patch.set_alpha(0.0)
            ax.set_axis_off()
            pos = ax.get_position().bounds
            offset = 0.013
            ax.set_position([pos[0]-offset, pos[1]-offset, pos[2]+2*offset, pos[3]+2*offset])

            # fig.subplots_adjust(left=0, bottom=0.25)
            l = []
            for edge in edges:
                v0 = vertices(xy, xz, xw, yz, yw, zw)[edge[0]]
                v1 = vertices(xy, xz, xw, yz, yw, zw)[edge[1]]
                # v0 = v0_debug[edge[0]]
                # v1 = v1_debug[edge[1]]
                x_coord = [v0[0], v1[0]]
                y_coord = [v0[1], v1[1]]
                l.append(ax.plot(x_coord, y_coord, 'k', solid_capstyle = 'round')[0])
    # plt.tight_layout()
    fig.savefig(datetime.datetime.now().strftime("%Y%m%d_%H%M%S") + '.svg')
    plt.show()


def main():
    vertices = []
    for i in [-1, 1]:
        for j in [-1, 1]:
            for k in [-1, 1]:
                for l in [-1, 1]:
                    vertices.append(np.array([i, j, k, l]))

    assert np.shape(vertices) == (16, 4)

    vertices_4d = vertices

    edges = []
    for i in range(0, 16):
        for j in range(i + 1, 16):
            if (np.linalg.norm(vertices[i] - vertices[j])) <= 2:
                edges.append([i, j])

    def rotation(xy, xz, xw, yz, yw, zw):
        tmp = np.dot(
            rot_zw(zw),
            np.dot(
                rot_xw(yw),
                np.dot(rot_yw(xw),
                       np.dot(rot_xy(xy), np.dot(rot_xz(xz), rot_yz(yz))))))
        return tmp

    def vertices_2d(xy, xz, xw, yz, yw, zw):
        tmp = proj_xy(
            np.transpose(
                np.dot(rotation(xy, xz, xw, yz, yw, zw),
                       np.transpose(vertices_4d))))
        assert np.shape(tmp) == (16, 2), np.shape(tmp)
        return tmp

    # plot_tesseract_slider(vertices_2d, edges)
    plot_tesseract_grid(vertices_2d, edges)


main()
